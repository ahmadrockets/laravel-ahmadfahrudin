<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\DocumentController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function (Request $request) {
    return response()->json(['message' => 'Laravel - Ahmad Fahrudin']);
});
Route::post('register', [UserController::class, 'register']);
Route::post('login', [UserController::class, 'login']);

Route::middleware(['jwt.verify'])->prefix('document-service')->group(function () {
    Route::get('/', [DocumentController::class, 'getalldocument']);
    Route::post('folder', [DocumentController::class, 'setfolder']);
    Route::get('folder/{folderid}', [DocumentController::class, 'getfilesbyfolder']);
    Route::delete('folder', [DocumentController::class, 'deletefolder']);
    Route::post('document', [DocumentController::class, 'setfile']);
    Route::get('document/{folderid}', [DocumentController::class, 'getdocumentbyid']);
    Route::delete('document', [DocumentController::class, 'deletedocument']);
});