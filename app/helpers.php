<?php

function jwttoarray($authorization)
{
  $response = explode('.', $authorization);
  $token    = trim($response[1]);
  $user     = json_decode(base64_decode($token), TRUE);
  return $user;
}
