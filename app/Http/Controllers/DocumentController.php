<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Document;
use App\Models\File;
use JwtAuth;

class DocumentController extends Controller
{
    public function getalldocument(Request $request)
    {
        $document = Document::all();
        $res = [];
        foreach ($document as $row) {
           $res[] = [
                "id"         => $row->id,
                "name"       => $row->name,
                "type"       => $row->type,
                "is_public"  => $row->is_public,
                "owner_id"   => $row->owner_id,
                "share"      => json_decode($row->share, TRUE),
                "timestamp"  => $row->timestamp,
                "company_id" => $row->company_id
           ];
        }
        return response()->json(['error' => 'false', 'data'=>$res]);
    }
    public function setfolder(Request $request)
    {
        $payload = json_decode(request()->getContent(), true);
        $res     = [];
        $user    = jwttoarray($request->header('Authorization'));

        $model = Document::find($payload['id']);
        if (!$model) {
            $model = new Document;
        }
        $model->id         = $payload['id'];
        $model->name       = $payload['name'];
        $model->type       = "folder";
        $model->is_public  = 1;
        $model->owner_id   = (int)$user['user_id'];
        $model->share      = "[]";
        $model->timestamp  = 1;
        $model->company_id = (int)$user['company_id'];

        if($model->save()){
            $res = [
                "id"         => $model->id,
                "name"       => $model->name,
                "type"       => $model->type,
                "content"    => (object)[],
                "timestamp"  => $model->timestamp,
                "owner_id"   => $model->owner_id,
                "company_id" => $model->company_id
           ];
            return response()->json(['error' => 'false', 'data' => $res]);
        }else{
            return response()->json(['error' => 'true', 'data' => []]);
        }
    }
    public function deletefolder(Request $request)
    {
        $payload = json_decode(request()->getContent(), true);
        $model = Document::find($payload['id']);
        if(!$model){
            return response()->json(['error' => 'true', 'message' => 'Folder not found']);
        }
        if($model->delete()){
            return response()->json(['error' => 'false', 'message' => 'Success delete folder']);
        }else{
            return response()->json(['error' => 'true', 'message' => 'Failed delete folder']);
        }
    }
    public function getfilesbyfolder($folderid)
    {
        $model = File::where('folder_id',$folderid)->get();
        $res = [];
        foreach ($model as $row) {
            $res[] = [
                "id"        => $row->id,
                "name"      => $row->name,
                "type"      => $row->type,
                "folder_id" => $row->folder_id,
                "content"   => json_decode($row->content, TRUE),
                "timestamp" => $row->timestamp,
                "owner_id"  => $row->owner_id,
                "share"     => json_decode($row->share, TRUE),
            ];
        }
        return response()->json(['error' => 'false', 'data' => $res]);
    }
    public function setfile(Request $request)
    {
        $payload = json_decode(request()->getContent(), true);

        $model = File::find($payload['id']);
        if(!$model){
            $model = new File;
        }
        $model->id         = $payload['id'];
        $model->name       = $payload['name'];
        $model->type       = $payload['type'];
        $model->folder_id  = $payload['folder_id'];
        $model->content    = json_encode($payload['content'], TRUE);
        $model->timestamp  = $payload['timestamp'];
        $model->owner_id   = $payload['owner_id'];
        $model->share      = json_encode($payload['share'],TRUE);
        $model->company_id = $payload['company_id'];
        if($model->save()){
            $res = [
                "id"         => $model->id,
                "name"       => $model->name,
                "type"       => $model->type,
                "folder_id"  => json_decode($model->content, TRUE),
                "content"    => $model->content,
                "timestamp"  => $model->timestamp,
                "owner_id"   => $model->owner_id,
                "share"      => json_decode($model->share, TRUE),
                "company_id" => $model->company_id
           ];
            return response()->json(['error' => 'false', 'message'=> 'Success set document', 'data' => $res]);
        }else{
            return response()->json(['error' => 'true', 'message' => 'Failed set document', 'data' => []]);
        }
    }
    public function getdocumentbyid($documentid)
    {
        $model = File::find($documentid);
        $res = [
            "id"         => $model->id,
            "name"       => $model->name,
            "type"       => $model->type,
            "folder_id"  => json_decode($model->content, TRUE),
            "content"    => $model->content,
            "timestamp"  => $model->timestamp,
            "owner_id"   => $model->owner_id,
            "share"      => json_decode($model->share, TRUE),
            "company_id" => $model->company_id
        ];
        return response()->json(['error' => 'false', 'message' => 'Success get document', 'data' => $res]);
    }
    public function deletedocument(Request $request)
    {
        $payload = json_decode(request()->getContent(), true);
        $model   = File::find($payload['id']);
        if(!$model){
            return response()->json(['error' => 'true', 'message' => 'Document not found']);
        }
        if($model->delete()){
            return response()->json(['error' => 'false', 'message' => 'Success delete document']);
        }else{
            return response()->json(['error' => 'true', 'message' => 'Failed delete document']);
        }
    }
}
