<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->string('id', 100);
            $table->string('name', 255);
            $table->string('type', 10);
            $table->string('folder_id', 100);
            $table->text('content')->nullable();
            $table->integer('timestamp')->nullable();
            $table->integer('owner_id')->default(0);
            $table->string('share', 255)->default('[]');
            $table->integer('company_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
